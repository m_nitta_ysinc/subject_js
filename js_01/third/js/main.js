//hoverしたときにJavaScript版の大きさを変更する挙動(アニメーション付き)
window.onload = () => {
	const target = document.getElementById('target');
	const list = target.classList;
	console.log(list);

	target.addEventListener('mouseenter', () => {
	　　target.classList.add('size_up');
	}, false);


	target.addEventListener('mouseleave', () => {
		target.classList.remove('size_up')
	}, false);
}