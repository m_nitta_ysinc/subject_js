document.addEventListener('DOMContentLoaded', function(){
	const tabs = document.getElementsByTagName('li');
	for(var i = 0; i < tabs.length; i++) {
    	tabs[i].addEventListener('click', tabClick, false);
	}
   	tabs[0].classList.add('active');
})


let tabClick = (event) =>{
	const tab = event.target;
	const data = tab.dataset.img;
	const image = document.getElementById('image');
	image.src = data;
	const tabs = document.getElementsByTagName('li');
	for (var i = 0; i < tabs.length; i++) {
		tabs[i].classList.remove('active');
	}
	tab.classList.add('active');
}