//モーダルを開く動き
let open = () => {
	const modal = document.getElementById('modalPanel');
	const black = document.getElementById('blackout');
	modal.style.display = 'block';
	black.style.display = 'block';
}

//モーダルを閉じる挙動
let close = () =>{
	const modal = document.getElementById('modalPanel');
	const black = document.getElementById('blackout');
	modal.style.display = 'none';
	black.style.display = 'none';
}

//モーダルの中身の色とテキストが変わる挙動
let color = () =>{
	const obj = event.target.getAttribute('class');
	const text = document.getElementById('default');
	const ul = event.target.parentNode;
	const li = ul.querySelectorAll('li');
	const n = Array.prototype.indexOf.call(li, event.target) + 1;
	text.setAttribute('class', obj);
	text.innerHTML = 'ダイアログの中身です' + n;
}

window.onload = () =>{
	const btn = document.getElementById('modal');
	btn.addEventListener('click', () => {
		open();
	})

	const closeBtn = document.getElementById('close');
	closeBtn.addEventListener('click', () => {
		close();
	})

	const ul = document.getElementById('color');
	ul.addEventListener('click', () => {
		color();
	})
}